#Simple peer to peer network

###Introduction
p2p is a simple application and you can share files between peers
by UDP and python socket library.

###How to join network
For joining the network you need a .p2p (just like joining torrent network)
that is information of one or many of the peers running currently on network.
by running the script, application will look for a "peer.p2p" and try to connect
to the network information in that file and if it couldn't connect to any, it will
start a network for itself

###How to find a peer.p2p file
After running the first peer with an empty "peer.p2p" file, script will generate a 
"share.p2p" file that would contain information of peer that just created or joined 
network. So anyone who wants to be in this network gets the "share.p2p" file from
someone on network and copy its content to "peer.p2p" file and run the script.

##How to run p2p script
#####server
For running it as some peer who uploads file:
######python3 main.py --serve --name {file_name} --path {file_path} --port {listen_port:default=1234}

#####receiver
For downloading a file from network:
######python3 main.py --receive {file_name}
