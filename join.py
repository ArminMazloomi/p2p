import logging
import socket
import json

from socketapi import BUFF_SIZE

# change these variables to run on other ports
IP = 'localhost'

WAIT_TIMEOUT = 5


def _get_lines(peer_file_path: str):
    f = open(peer_file_path, 'r')
    return f.readlines()


def _get_ip_port(l):
    ip = l.split(' ')[0]
    port = int(l.split(' ')[1])
    return ip, port


def _try_to_connect(addr: tuple, stay: bool, listen_port: int):
    req = json.dumps({
        'type': 'join',
        'content': {
            'peer_addr': {
                'ip': IP,
                'port': listen_port
            },
            'stay': stay
        }
    }).encode('utf-8')
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
    s.sendto(req, addr)
    s.settimeout(WAIT_TIMEOUT)
    data, add = s.recvfrom(BUFF_SIZE)
    result = json.loads(data.decode('utf-8'))['peers_address']
    s.close()
    return result


def join_network(peer_file_path: str, stay=True, listen_port=1234):
    lines = _get_lines(peer_file_path)
    for l in lines:
        ip, port = _get_ip_port(l)
        try:
            network_info = _try_to_connect((ip, port), stay, listen_port)
        except socket.gaierror as e:
            continue
        if network_info:
            break
    if not network_info:
        raise Exception('Could not resolve the peer')
    return network_info
