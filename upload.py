import socket
import logging
import json


from os import path

from join import (
    IP)
from socketapi import (
    PEERS_ADDRESS, send_to_all_and_close, BUFF_SIZE, NULL_FILE, EOT
)

NETWORK_NODE_SHARE_FILE = 'share.p2p'
SHARED_FILES = set()


def _update_shared_files(*args):
    global SHARED_FILES
    SHARED_FILES.update(args)


def _find_file(file_name):
    for f in SHARED_FILES:
        if f[0] == file_name:
            if path.exists(f[0]):
                f = open(f[0], 'rb')
                return f
    return None


def _send_file(s, addr, file):
    while True:
        file_part = file.readline(BUFF_SIZE)
        logging.debug('sending {} bytes'.format(len(file_part)))
        if file_part:
            s.sendto(file_part, addr)
        else:
            s.sendto(EOT, addr)
            return True


class RequestHandler:

    @staticmethod
    def introduce(s, addr, content):
        addr = (content['peer_addr']['ip'], content['peer_addr']['port'])
        PEERS_ADDRESS.add(addr)

    @staticmethod
    def join(s, addr, content):
        ip = content['peer_addr']['ip']
        port = content['peer_addr']['port']
        if content['stay']:
            PEERS_ADDRESS.add((ip, port))
            send_to_all_and_close(json.dumps({
                'type': 'introduce',
                'content': content
            }))
        s.sendto(json.dumps({
            'peers_address': list(PEERS_ADDRESS)
        }).encode('utf-8'), addr)
        return True

    @staticmethod
    def file(s, addr, content):
        file_name = content['file_name']
        b_file = _find_file(file_name)
        if b_file:
            logging.debug('{} found'.format(file_name))
            return _send_file(s, addr, b_file)
        else:
            s.sendto(NULL_FILE, addr)
            return False


def _ingest_request(s, addr, req):
    handler = getattr(RequestHandler, req['type'])
    return handler(s, addr, content=req['content'])


def _create_join_file(listen_port):
    f = open(NETWORK_NODE_SHARE_FILE, 'w+')
    f.truncate(0)
    f.write('{} {}'.format(IP, listen_port))


def serve_files(listen_port, *args):
    _create_join_file(listen_port)
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    logging.debug('socket created successfully')
    try:
        s.bind((IP, listen_port))
    except OSError:
        return
    _update_shared_files(*args)
    logging.info('listening on {}:{}'.format(IP, listen_port))
    try:
        while True:
            logging.info('all peers ==> {}'.format(PEERS_ADDRESS))
            req, addr = s.recvfrom(BUFF_SIZE)
            logging.debug(req)
            is_success = _ingest_request(s, addr, json.loads(req.decode('utf-8')))
            if is_success:
                logging.info('upload successful')
            else:
                logging.info('upload failed')
    except KeyboardInterrupt:
        pass
