import logging
import json
import socket

from socketapi import (
    BUFF_SIZE, NULL_FILE, PEERS_ADDRESS, EOT
)

SAVED_FILES_PATH = 'savedfiles/'


def _open_new_file(file_name):
    f = open(SAVED_FILES_PATH + file_name, 'wb')
    f.truncate(0)
    return f


def _get_file(s: socket, initial_packet, file_name):
    f = _open_new_file(file_name)
    f.write(initial_packet)
    while True:
        res, addr = s.recvfrom(BUFF_SIZE)
        if res == EOT:
            f.close()
            return True
        f.write(res)


def _request_file(file_name):
    logging.debug('requesting file')
    content = json.dumps({
        'type': 'file',
        'content': {
            'file_name': file_name
        }
    })
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
    for addr in PEERS_ADDRESS:
        logging.debug('trying {}'.format(addr))
        s.sendto(content.encode('utf-8'), addr)
        logging.debug('sent')
        res, addr = s.recvfrom(BUFF_SIZE)
        logging.debug(res)
        if res != NULL_FILE:
            return _get_file(s, res, file_name)
    return None


def download(file_name):
    logging.info('started downloading {}'.format(file_name))
    res = _request_file(file_name)
    if res:
        logging.info('download completed')
    else:
        logging.info('file not found')
