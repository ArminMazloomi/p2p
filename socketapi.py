import socket

BUFF_SIZE = 4096
NULL_FILE = b'0x0'
EOT = '$'.encode('utf-8')
# set of tuples (ip, port)
PEERS_ADDRESS = set()


def _send_to_all(content: str):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
    for addr in PEERS_ADDRESS:
        try:
            s.sendto(content.encode('utf-8'), addr)
        except socket.gaierror as e:
            print(e)
    return s


def send_to_all_and_close(content: str):
    s = _send_to_all(content)
    s.close()


def send_to_all_and_get_response(content: str):
    s = _send_to_all(content)
    res = s.recvfrom(BUFF_SIZE)
    s.close()
    return res.decode('utf-8')


def process_network_info(info):
    PEERS_ADDRESS.update([tuple(l) for l in info])
